import "./App.css";
import React from "react";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quote: quotes[initRandomIndex],
    };
    this.setNewRandomQuote = this.setNewRandomQuote.bind(this);
  }
  setNewRandomQuote = () => {
    let randomIndex = Math.floor(Math.random() * quotes.length);
    this.setState({
      quote: quotes[randomIndex],
    });
  };

  render() {
    return (
      <div id="quote-box">
        <div id="text">"{this.state.quote.quote}"</div>
        <div id="author">{this.state.quote.author}</div>
        <a
          id="tweet-quote"
          href={`https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=${this.state.quote.quote} ${this.state.quote.author}`}
        >
          tweet it
        </a>
        <input
          id="new-quote"
          value="new quote"
          type="button"
          onClick={this.setNewRandomQuote}
        />
      </div>
    );
  }
}

const quotes = [
  {
    quote: "Be yourself; everyone else is already taken.",
    author: "Oscar Wilde",
  },
  {
    quote:
      "I'm selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can't handle me at my worst, then you sure as hell don't deserve me at my best.",
    author: "Marilyn Monroe",
  },
  {
    quote:
      "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.",
    author: "Albert Einstein",
  },
  {
    quote:
      "Be who you are and say what you feel, because those who mind don't matter, and those who matter don't mind.",
    author: "Bernard M. Baruch",
  },
  {
    quote:
      "You know you're in love when you can't fall asleep because reality is finally better than your dreams.",
    author: "Dr. Seuss",
  },
];
let initRandomIndex = Math.floor(Math.random() * quotes.length);
